# Discord Status Helper

Since Discord doesn't provide much customization options for its "Playing ..." status field under the username,
much less on Linux than on Windows, I've made this. It allows you to configure your Discord status based on the open
windows (requires X11, therefore only works on Linux!). Currently, the only file that gets read is `conf/intellij.toml`
although I will make it read the entire directory in the future. That file should be sufficiently commented.

### Building

To build this application, make sure `rustup` is installed on your system.  
If this is your first time using Rust (or you aren't using the nightly build), use the following commands to set up the toolchain:

    % rustup install nightly
    % rustup default nightly

To compile the files, execute the following command:

    % cargo build --release

If it returns without any errors, you're good to go. To run the program, execute `./dsh`. Any changes to the config file
must be followed by restarting the program since it only reads it once.

This is my first program written in Rust, so there might be some serious style issues. The functionality is there, though
(I'm using this myself) :P

### Configuration
Each `entry` must have a `name`. The `priority` is optional and defaults to 0. The priority with the highest number is
the most important one, the one which will "win" if there are multiple matching entries.
An entry can have multiple or none `condition`s, which must have a `field` where it should get it's data from (check the
command `xprop` for what the possible values `WM_CLASS`, `WM_CLASS1`, `WM_NAME` mean). A condition also must have an
`expr` which is a regular expression that needs to match the contents of the `field` for the condition to be true.
Normally, all conditions of an entry must be true for that entry to be activated. However, you can specify a `group`
in 2 or more conditions so that one or more condition of a group must be true for that group to be considered active.  

This sounds probably pretty convoluted, so here's some pseudo code:

    def Condition.is_active() = X11.get_window_props().map(it => it.get(this.field))
                                                  .any(it => it.matches(this.expr))
    
    def Group.is_active() = this.conditions.any(it => it.is_active()) // OR connection for conditions in a group
    
    def Entry.is_active() = this.groups.all(it => it.is_active()) // AND connection for groups in an entry