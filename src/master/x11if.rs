// The X11 interface.

use x11::xlib::{self, Display, Window, XClassHint, Atom};
use std::ptr::{null, null_mut};
use std::vec::Vec;
use std::os::raw::{c_void, c_char, c_uchar, c_int, c_uint, c_ulong};
use std::ffi::CStr;
use std::collections::HashMap;
use std::result::Result;

pub fn scan() -> Vec<HashMap<String, String>> {
  let mut vec = Vec::new();
  unsafe {
    let disp: *mut Display = xlib::XOpenDisplay(null());

    let rootw: Window = xlib::XDefaultRootWindow(disp);

    for win in get_window_children_rec(disp, rootw) {
      vec.push(get_props(disp, win))
    }

    xlib::XCloseDisplay(disp);
  }
  vec
}

unsafe fn get_props(disp: *mut Display, window: Window) -> HashMap<String, String> {
  let mut map = HashMap::new();

  match get_window_class(disp, window) {
    Ok((wmclass, wmclass1)) => {
      map.insert("WM_CLASS".into(), wmclass);
      map.insert("WM_CLASS1".into(), wmclass1);
    }
    Err(_) => ()
  }

  match get_wm_name(disp, window) {
    Ok(wmname) => { map.insert("WM_NAME".into(), wmname); }
    Err(_) => ()
  }

  map
}

unsafe fn get_window_class(disp: *mut Display, window: Window) -> Result<(String, String), i32> {
  let class_hints: *mut XClassHint = xlib::XAllocClassHint();
  let res: c_int = xlib::XGetClassHint(disp, window, class_hints);
  if res == xlib::True {
    let wmclass = CStr::from_ptr((*class_hints).res_class).to_string_lossy().into_owned();
    let wmclass1 = CStr::from_ptr((*class_hints).res_name).to_string_lossy().into_owned();

    xlib::XFree((*class_hints).res_class as *mut c_void);
    xlib::XFree((*class_hints).res_name as *mut c_void);
    xlib::XFree(class_hints as *mut c_void);

    Ok((wmclass, wmclass1))
  } else {
    Err(res)
  }
}

unsafe fn get_wm_name(disp: *mut Display, window: Window) -> Result<String, i32> {
  let mut actual_type: Atom = 0;
  let mut actual_format: c_int = 0;
  let mut nitems: c_ulong = 0;
  let mut bytes_after: c_ulong = 0;
  let mut prop: *mut c_uchar = null_mut();

  let res: c_int = xlib::XGetWindowProperty(disp, window, xlib::XA_WM_NAME, 0, 4096, xlib::False, xlib::XA_STRING,
                                            &mut actual_type, &mut actual_format, &mut nitems, &mut bytes_after, &mut prop);

  if actual_type == xlib::XA_STRING {
    let wmname = CStr::from_ptr(prop as *mut c_char).to_string_lossy().into_owned();
    Ok(wmname)
  } else {
    Err(res)
  }
}

unsafe fn get_window_children_rec(disp: *mut Display, window: Window) -> Vec<Window> {
  let mut vec = vec![window];
  get_window_children(disp, window).iter()
    .flat_map(|x| get_window_children_rec(disp, *x))
    .for_each(|x| vec.push(x));
  vec
}

unsafe fn get_window_children(disp: *mut Display, window: Window) -> Vec<Window> {
  let mut root: Window = 0;
  let mut parent: Window = 0;
  let mut children: *mut Window = null_mut();
  let mut children_count: c_uint = 0;

  xlib::XQueryTree(disp, window, &mut root, &mut parent, &mut children, &mut children_count);

  let result: Vec<Window> = (0..children_count).map(|x| *children.offset(x as isize)).collect();

  if children_count != 0 {
    xlib::XFree(children as *mut c_void);
  }

  result
}